const Pool = require('pg').Pool
const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
});


const getPropertyClassifieds = async () => {
  try {
    return await new Promise(function (resolve, reject) {
      pool.query("SELECT * FROM property_classified", (error, results) => {
        if (error) {
          reject(error);
        }
        if (results && results.rows) {
          resolve(results.rows);
        } else {
          reject(new Error("No results found"));
        }
      });
    });
  } catch (error_1) {
    console.error(error_1);
    throw new Error("Internal server error");
  }
};

const createPropertyClassified = (body) => {
  return new Promise(function (resolve, reject) {
    const { title, type, area, price, extraDescription } = body;
    pool.query(
      "INSERT INTO property_classified (title, type, place_id, price, description) VALUES ($1, $2, $3, $4, $5) RETURNING *",
      [title, type, area, price, extraDescription],
      (error, results) => {
        if (error) {
          reject(error);
        }
        if (results && results.rows) {
          resolve(
            `A new classified has been added: ${JSON.stringify(results.rows[0])}`
          );
        } else {
          reject(new Error("No results found"));
        }
      }
    );
  });
};

const deletePropertyClassified = (id) => {
  return new Promise(function (resolve, reject) {
    pool.query(
      "DELETE FROM property_classified WHERE id = $1",
      [id],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`Classified deleted with ID: ${id}`);
      }
    );
  });
};

module.exports = {
  getPropertyClassifieds,
  createPropertyClassified,
  deletePropertyClassified
};
