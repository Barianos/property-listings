const request = require('supertest');
const app = require('./index.js'); // Import your Express app

const { getPropertyClassifieds, createPropertyClassified } = require('./propertyClassifiedsModel');

describe('Property Classifieds Model Tests', () => {
  test('getPropertyClassifieds function should return all property classifieds', async () => {
      const response = await request(app).get('/properties');
      expect(response.status).toBe(200);

  });

  test('createPropertyClassified function should create a new property classified', async () => {
    const newClassified = {
        title: 'New Classified',
        type: 'rent',
        place_id: 'Area ID',
        price: 1000,
        extraDescription: 'Extra description'
      };
      const response = await request(app)
        .post('/properties/add')
        .send(newClassified);
      expect(response.status).toBe(200);
      console.log(response)
      expect(response.text).toContain(`"title":"New Classified"`)
    });

});
