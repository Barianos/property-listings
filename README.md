## Property Listings

## Description
This project is a prototype of a very small functionality. The goal is to have a React App that allows users to add classifieds about properties, and those classifieds to be saved in a PostgreSQL database through a Node.JS API.


## Installation & Execution
To successfully execute this project you will need a PostgreSQL 14 installation on your system as well as Node.js.

Follow these steps to run the project on your local machine:
### Database Migration

1. Navigate to the `db` directory:
    ```bash
    cd db
    ```

2. Run the database migration script to set up the database schema:
    ```bash
    db-migrate up
    ```

### Start API & Front-end

3. Navigate back to the root folder:
    ```bash
    cd ..
    ```

4. Run the backend API server:
    ```bash
    npm start
    ```

### Start Projects Separately
If you prefer to start only one of the projects run the corresponding command

6. Navigate to the root folder if you're not already there:
    ```bash
    cd path/to/root/folder
    ```

7. Run the command to start the frontend React app:
    ```bash
    npm run start:frontend
    ```

8. Run the command to start the API:
    ```bash
    npm run start:api
    ```

## Run Tests
### API tests
1. Navigate to the `api` directory:
    ```bash
    cd api
    ```
2. Run the tests command:
    ```bash
    npm test
    ```
### Front End Tests
1. Navigate to the `api` directory:
    ```bash
    cd front-end
    ```
2. Run the tests command:
    ```bash
    npm test
    ```

## Packages Used

### Backend:
- **express**: Web application framework for Node.js
- **pg**: PostgreSQL client for Node.js
- **dotenv**: Loads environment variables from a .env file into process.env

### Frontend:
- **material-ui**: a visually appealing and interactive user interface.
- **axios**: Promise-based HTTP client for the browser and Node.js
- **cors-anywhere**: CORS proxy server to bypass CORS restrictions
