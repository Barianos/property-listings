import React from 'react';
import { render, screen, fireEvent  } from '@testing-library/react';
import AreasAutocomplete from './AreasAutocomplete';
import axios from 'axios'; // Import axios module

jest.mock('axios');

describe('AreasAutocomplete', () => {
  test('renders without crashing', () => {
    render(<AreasAutocomplete />);

    // Check if the component renders without errors
    const autoCompleteElement = screen.getByTestId('areas-autocomplete');
    expect(autoCompleteElement).toBeInTheDocument();
  });

  it('fetches options and displays them after user input', async () => {
    const mockedOptions = [{ mainText: 'Option1', secondaryText: 'Desc1' }, { mainText: 'Option2', secondaryText: 'Desc2' }];
    axios.get.mockResolvedValueOnce({ data: mockedOptions });

    render(<AreasAutocomplete />);

    const input = screen.getByRole('textbox');
    fireEvent.change(input, { target: { value: 'London' } });

    await screen.findByText('Option1, Desc1');
    expect(screen.getByText('Option2, Desc2')).toBeInTheDocument();
  });
});
