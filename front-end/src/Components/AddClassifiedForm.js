import React, { useState } from 'react';
import { TextField, Button, Select, MenuItem, FormControl, InputLabel, TextareaAutosize, Grid, Typography } from '@mui/material';
import AreasAutoComplete from './AreasAutocomplete';
import '../styles/styles.css';
import axios from 'axios';


const AddClassifiedForm = () => {

  const [formData, setFormData] = useState({
    title: '',
    type: '',
    area: null,
    price: '',
    extraDescription: ''
  });

  const [errors, setErrors] = useState({});


  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleAreaChange = (selectedArea) => {
    setFormData({ ...formData, area: selectedArea.placeId });
  };

  const validateForm = () =>{ // Perform form validation
    const validationErrors = {};
    if (!formData.title.trim()) {
      validationErrors.title = 'Title is required';
    }
    if (!formData.type.trim()) {
      validationErrors.type = 'Type is required';
    }
    if (!formData.area.trim()) {
      validationErrors.area = 'Area is required';
    }
    if (!formData.price.trim()) {
      validationErrors.price = 'Price is required';
    } else if (isNaN(formData.price)) {
      validationErrors.price = 'Price must be a number';
    }
    setErrors(validationErrors);
    return validationErrors;
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const validationErrors = validateForm();
    // If no validation errors, submit the form
    if (Object.keys(validationErrors).length === 0) { // No validation errors -> submit the form
      saveFormData(formData);// Send form data to API
    }
  };

  const saveFormData = async (formData) => {
    const response = await axios.post('http://localhost:3001/properties/add', formData);

    if (response.status === 200) {
      console.log('Form data saved successfully:', response.data);
    }

    setFormData({     // Reset form fields
      title: '',
      type: '',
      area: null,
      price: '',
      extraDescription: ''
    });
  };


  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={2} padding={5}>
      <Grid item xs={12}>
      <Typography variant="h3" gutterBottom>
      New Property Classified
    </Typography>
      </Grid>
        <Grid item xs={12} >
          <TextField className="commonStyle"
            name="title"
            value={formData.title}
            onChange={handleChange}
            label="Title"
            variant="outlined"
            required
            inputProps={{ maxLength: 155 }}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth variant="outlined" className="commonStyle">
            <InputLabel>Type</InputLabel>
            <Select
              name="type"
              value={formData.type}
              onChange={handleChange}
              label="Type"
            >
              <MenuItem value="">Select Type</MenuItem>
              <MenuItem value="rent">Rent</MenuItem>
              <MenuItem value="buy">Buy</MenuItem>
              <MenuItem value="exchange">Exchange</MenuItem>
              <MenuItem value="donation">Donation</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} className="commonStyle">
          <AreasAutoComplete area={formData.area} onAreaChange={handleAreaChange} />
          {errors.area && <Typography color="error">{errors.area}</Typography>}
        </Grid>
        <Grid item xs={12}>
          <TextField
            name="price"
            value={formData.price}
            onChange={handleChange}
            label="Price (in euros)"
            variant="outlined"
            type="number"
            required
            className="commonStyle"
          />
        </Grid>
        <Grid item xs={12} >
          <TextareaAutosize
            name="extraDescription"
            value={formData.extraDescription}
            onChange={handleChange}
            rows={4}
            placeholder="Extra Description"
            className="commonStyle"
          />
        </Grid>
        <Grid item xs={12 }>
          <Button type="submit" variant="contained" color="primary">
            Submit
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddClassifiedForm;
