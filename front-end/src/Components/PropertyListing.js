import React from 'react';
import {
  Card,
  CardActions,
  CardContent,
  Grid,
  Typography,
  Button,
  Tooltip,
} from '@mui/material';

const PropertyListing = ({ property }) => {
  if (!property) {
    return <Typography>Loading...</Typography>;
  }

  return (
    <Grid item xs={12} spacing={2} padding={5} key={property.id}>
      <Card variant="outlined">
        <CardContent>
          <Typography variant="h5" color="text.secondary" gutterBottom>
            {property.title}
          </Typography>
          <Typography variant="body" color="text.secondary">
            <Tooltip title={property.type}>
              <span>{property.type}</span>
            </Tooltip>
            -
            {property.price}
          </Typography>
          <Typography variant="body2">
            {property.description}
          </Typography>
        </CardContent>
        <CardActions>

        <Button>
          Contact
        </Button>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default PropertyListing;
