import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Grid } from '@mui/material';

import PropertyListing from './PropertyListing';
import axios from 'axios';

const Home = () => {
  const [properties, setProperties] = useState([]);

  useEffect(() => { // Fetch property data when component mounts
    const fetchProperties = async () => {
      try {
        const response = await axios.get('http://localhost:3001/properties/');
        setProperties(response.data);
      } catch (error) {
        console.error('Error fetching properties:', error);
      }
    };

    fetchProperties();
  }, []);

  return (
    <div>
      <h1>Property Listings <Button component={Link} to="/add" variant="contained" color="primary">
        Add Classified
      </Button></h1>

      <Grid container spacing={2}>
        {/* Map through properties and render PropertyListing component for each */}
        {properties.map(property => (
          <Grid item xs={12} key={property.id}>
            <PropertyListing property={property} />
          </Grid>
        ))}
      </Grid>

    </div>
  );
};

export default Home;
