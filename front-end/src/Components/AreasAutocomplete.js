import React, { useState, useEffect } from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import axios from 'axios';

const cache = {};

const AreasAutoComplete = ({ area, onAreaChange }) => {
  const CORS_BYPASS = `https://cors-anywhere.herokuapp.com/`;
  const AUTOCOMPLETE_API_URL = `https://4ulq3vb3dogn4fatjw3uq7kqby0dweob.lambda-url.eu-central-1.on.aws/?input=`;
  const [options, setOptions] = useState([]);
  const [selectedArea, setSelectedArea] = useState(area);

 useEffect(() => {
    setSelectedArea(area); // Update selectedArea when area prop changes
  }, [area]);

  const fetchOptions = async (inputValue) => {
    console.log(cache)
    if (cache[inputValue]) { // Return cached results if available
      setOptions(cache[inputValue]);
      return;
    }
    try {
      const response = await axios.get(CORS_BYPASS + AUTOCOMPLETE_API_URL + inputValue);
      if (response.data) {
        setOptions(response.data);
        cache[inputValue] = response.data; // Update cache with new results
      }
    } catch (error) {
      console.error('Error fetching options:', error);
    }
  };

  const handleAreaSelect = (newValue) => {
    setSelectedArea(newValue); // Update selectedArea when a new area is selected
    onAreaChange(newValue); // Pass the new area value to the parent component
  };

  return (
    <div data-testid="areas-autocomplete">
    <Autocomplete style={{ width: "80%" }}
      options={options}
      getOptionLabel={(option) => option.mainText + `, ` + option.secondaryText}
      onInputChange={(event, newInputValue) => {
        if (newInputValue.length >= 3) {
          fetchOptions(newInputValue);
        }
      }}
      onChange={(event, newValue) => {
        handleAreaSelect(newValue);
      }}
      renderInput={(params) => (
        <TextField {...params} label="Autocomplete" variant="outlined" inputProps={{
          ...params.inputProps,
          role: 'textbox',
          'aria-autocomplete': 'list',
        }} />
      )}
    />
    </div>
  );
};

export default AreasAutoComplete;
