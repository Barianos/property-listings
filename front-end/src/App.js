import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Home from './Components/Home';
import AddClassifiedForm from './Components/AddClassifiedForm';

const theme = createTheme({
  // Define your theme here
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Routes>
        <Route path="/" element={<Home />} />
          <Route path="/add" element={<AddClassifiedForm />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;
