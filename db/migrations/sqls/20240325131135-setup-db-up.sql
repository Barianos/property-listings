CREATE TYPE add_type AS ENUM (
  'rent',
  'buy',
  'exchange',
  'donation'
);

CREATE TABLE property_classified (
    id SERIAL UNIQUE PRIMARY KEY,
    title VARCHAR,
    type add_type,
    place_id VARCHAR,
    price NUMERIC,
    description VARCHAR
);
